import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
function Customers(){

    //history
    var history = useHistory(); 

    //array of customers
    var [custList, setCustList] = useState([{
        email: "ath@a.com",
        first_name: "Ath",
        last_name: "Dhamse",
        mob_no: 5456321476,
        dob: "2000-08-01",
        gender: "MALE",
        address: {
            pincode: 431006,
            city: "Aurangabad",
            state: "Maharashtra"
        },
        pincode: null,
        role: "ROLE_CUSTOMER"
    }]);
    
    //method to bring array of customer on page load
    useEffect(()=>{
        axios.get("http://localhost:8080/Project/admin/customers")
        .then((response)=>{
            var list = response.data;
            setCustList(list);
        })
    },[]);


    //delete Customer function
    var deleteCust=(args)=>{
        var email = args.target.value;
        var url = "http://localhost:8080/Project/admin/deleteCustomer/"+email;
        axios.delete(url)
        .then((response)=>{
            console.log(response.data);
        })
        window.location.reload();
    }

    var goBack=()=>{
        history.goBack();
    }

    //index variable
    var i=1;
    
    return(<div className="p-5">
        <h1><b><i>List of Customers</i></b></h1>
        <button class="btn col-2 btn-dark mx-4" onClick={goBack}>Back</button>
        <table class="table">
  <thead class="thead-dark">
    <tr class="table-primary">
      <th scope="col">Id</th>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th>
      <th scope="col">Email</th>
      <th scope="col">Mobile</th>
      <th scope="col">Date Of Birth</th>
      <th scope="col">Gender</th>
      {/* <th scope="col">Company Name</th> */}
      <th scope="col">PinCode</th>
      <th scope="col">City</th>
      <th scope="col">State</th>
    </tr>
  </thead>
  {custList.map((customer)=>{
    return(<tbody>
        <tr id={customer.email}>
        <th class="table-bg-transparent" scope="row">{i++}</th>
        <td class="table-bg-transparent"><b>{customer.first_name}</b></td>
        <td class="table-bg-transparent"><b>{customer.last_name}</b></td>
        <td class="table-bg-transparent"><b>{customer.email}</b></td>
        <td class="table-bg-transparent"><b>{customer.mob_no}</b></td>
        <td class="table-bg-transparent"><b>{customer.dob}</b></td>
        <td class="table-bg-transparent"><b>{customer.gender}</b></td>
        
        {/* <td class="table-danger">{vendor.cmp_name}</td> */}
        {/* <td class="table-danger">{customer.address.pincode}</td> */}
        <td class="table-bg-transparent"><b>{customer.address.pincode}</b></td>
        {/* <td class="table-danger">{customer.address.city}</td> */}
        <td class="table-bg-transparent"><b>{customer.address.city}</b></td>
        <td class="table-bg-transparent"><b>{customer.address.state}</b></td>
        {/* <button name="button" onClick={deleteCust} value={customer.email}>delete</button> */}
      </tr>
    </tbody>)
     })}
  </table>
  
      </div>)
 
    }

export default Customers;




{/* <table>
            <tr>
                <td>Id</td>
                <td>First Name</td>
                <td>Last Name</td>
                <td>email</td>
                <td>Mobile</td>
                <td>DOB</td>
                <td>Gender</td>
                <td>Pincode</td>
                <td>City</td>
                <td>State</td>
            </tr>
            {custList.map((customer)=>{
                return(
                    <tr id={customer.email}>
                <td>{i++}</td>
                <td>{customer.first_name}</td>
                <td>{customer.last_name}</td>
                <td>{customer.email}</td>
                <td>{customer.mob_no}</td>
                <td>{customer.dob}</td>
                <td>{customer.gender}</td>
                <td>{customer.address.pincode}</td>
                <td>{customer.address.city}</td>
                <td>{customer.address.state}</td>
                <td><button name="button"  onClick={deleteCust} value={customer.email}>delete</button></td>
            </tr>
                )
            })}
        </table> */}