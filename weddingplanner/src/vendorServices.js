import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";


function VendorServices() {
    //index 
    var i = 1;
    var history = useHistory();

    var [services, setServices] = useState([{
        servName: "CATERING",
        serv_desc: "Khaana Khazaana",
        serv_price: 1000.0,
        creatn_date: "2020-02-04",
        mod_date: "2020-02-04"
    }])

    var [addServ, setAddServ] = useState({
        servName: "",
        serv_desc: "",
        serv_price: 0
    });

    var [category, setCategory] = useState([]);

    //on page load bring in all services
    useEffect(() => {
        var url = "http://localhost:8080/Project/vendor/services/" + sessionStorage.getItem("email");
        axios.get(url)
            .then((response) => {
                var list = response.data;
                setServices(list);
            })
            .catch((err) => {
                console.log(err);
            })
    }, [])

    //on page load bring in all category
    useEffect(() => {
        var url = "http://localhost:8080/Project/admin/category";
        axios.get(url)
            .then((response) => {
                var list = response.data;
                setCategory(list);
            })
            .catch((err) => {
                console.log(err);
            })
    }, [])


    //delete Service
    var deleteService = (args) => {

    }

    var goBack=()=>{
        history.goBack();
    }

    //add Service
    var addService = () => {
        var data = { ...addServ };
        var url = "http://localhost:8080/Project/vendor/addservice/" + sessionStorage.getItem("email");
        axios.post(url, data)
            .then((response) => {
                var res = response.data;
                if(res==true){
                    window.location.reload();
                }else{
                    alert(res);
                }
        })
    }

    //OnChange handler
    var handleChange = (args) => {
        var copyOfService = { ...addServ };
        copyOfService[args.target.name] = args.target.value;
        setAddServ(copyOfService);
    }


    return (<div className="p-5 mb-1 mx-4">
        <h1><b><i>Your Services</i></b></h1>
        <button class="btn btn-dark mx-4" onClick={goBack}>Back</button>

        <table class="table">
        <thead class="thead-dark">
        <tr class="table-primary">
        <th scope="col">Id</th>
        <th scope="col">Service Name</th>
        <th scope="col">Description</th>
        <th scope="col">Price per Unit</th>
        <th scope="col">Date Created</th>
        <th scope="col">Date Modified</th>
            </tr>
            </thead>
            {services.map((o) => {
                return (<tbody>
                    <tr id={o.servName}>
                    <th class="table-bg-transparent" scope="row">{i++}</th>
                        
                        <td class="table-bg-transparent"><b>{o.servName}</b></td>
                        <td class="table-bg-transparent"><b>{o.serv_desc}</b></td>
                        <td class="table-bg-transparent"><b>{o.serv_price}</b></td>
                        <td class="table-bg-transparent"><b>{o.creatn_date}</b></td>
                        <td class="table-bg-transparent"><b>{o.mod_date}</b></td>
                        {/* <td><button value={o.servName} onClick={deleteService}>delete</button></td> */}
                    </tr>
                    </tbody>
                )
            })}

        </table>
        <hr></hr>
        <table class="table">
        <thead class="thead-dark">
        <tr class="table-primary">
       
        <th scope="col">Service Name</th>
        <th scope="col">Description</th>
        <th scope="col">Price per Unit</th>
        
            </tr>
            </thead>
                <tr>
                    <td><div class="form-group ">   
  <select name="servName" class="form-control bg-transparent" required value={addServ.servName} onChange={handleChange}>
                            <option value="">--Select Category--</option>
                            {category.map((a)=>{
                                return(
                                    <option value={a.servName}>{a.servName}</option> 
                                )   
                            })}
                        </select>
</div>
                    </td>
                    <td class="table-bg-transparent"><input type="text" name="serv_desc" value={addServ.serv_desc} onChange={handleChange}></input></td>
                    <td class="table-bg-transparent"><input type="number" name="serv_price" value={addServ.serv_price} onChange={handleChange}></input></td>
                    <td><button onClick={addService} class="btn btn-dark" >Add Service</button></td>
                </tr>
            
        </table>
    </div>)
}

export default VendorServices;