import axios from "axios";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import '../node_modules/bootstrap/dist/css/bootstrap.css'

function CustomerProfile(){
    
    var history = useHistory();
    var[customer, setCustomer] = useState({
        id:0,
        email: "",
        first_name: "",
        last_name: "",
        mob_no: 0,
        dob: "",
        gender: "",
        address: {
            pincode: 0,
            city: "",
            state: ""
        },
        role: "ROLE_CUSTOMER",
    });

    //OnChange handler
    var handleChange=(args)=>{
        var copyOfcustomer = {...customer};
        copyOfcustomer[args.target.name] = args.target.value;
        setCustomer(copyOfcustomer);
    }

    //on load bring customer profile details
    useEffect(()=>{
        var url = "http://localhost:8080/Project/customer/p/profile/"+sessionStorage.getItem("email");
        axios.get(url)
        .then((response)=>{
            var v = response.data;
            setCustomer(v);
        })
        .catch((err)=>{
            console.log(err);
        })
    },[])


    //array of list of states
    var[statesList, setStatesList] = useState([]);
    //array of list of cities
    var [cityList, setCityList] = useState([]);  
    // array of list of pincodes
    var [pincodes, setPincodes] = useState([]); 


     //on mount getting list of states
     useEffect(()=>{
        //call to get list of states
        debugger;
        getState();
           
    },[]);

    var getState = ()=>{
        axios.get("http://localhost:8080/Project/aux/state")
             .then(function(response){
                var states = response.data;
                setStatesList(states);
                // debugger;
             })
    }
    //On state select getting citylist
    var getCity = (args)=>{
        var statesSelected = args.target.value;
        var cityUrl = "http://localhost:8080/Project/aux/city/" + statesSelected;
        // debugger;
        axios.get(cityUrl)
             .then(function(response){
                var cities = response.data;
                setCityList(cities);
                // debugger;
             }) 
    }

    //On change city list
    var getPincode = (args)=>{
        var citySelected = args.target.value;
        var pinUrl = "http://localhost:8080/Project/aux/pincode/" + citySelected;
        // debugger;
        axios.get(pinUrl)
             .then(function(response){
                var pincode = response.data;
                setPincodes(pincode);
                // debugger;
             }) 
    }


    //function to delete profile
    var deleteProfile =()=>{
       var url = "http://localhost:8080/Project/customer/p/profile/"+sessionStorage.getItem("email");
        axios.delete(url)
        .then((response)=>{
            var result = response.data;
            if(result){
                history.push('/login');
            }
        })
    }

    //update profile function
    var updateProfile=(args)=>{
        var data = {...customer};

        var url = "http://localhost:8080/Project/customer/p/profile/"+sessionStorage.getItem("email");

        axios.put(url, data)
        .then((response)=>{
            var result = response.data;
            if(result){
                window.location.reload();
            }
            
        })
        .catch((err)=>{
            console.log(err);
        })


    }


    var goBack=()=>{
        history.goBack();
    }

    return(<div className="p-5 mb-3 mx-4">
        <h1><b><i>Customer Profile</i></b></h1>
        <div style={{width: 600}}>
    <button class="btn btn-danger mx-4 my-3" onClick={deleteProfile}>Delete Profile</button><button onClick={goBack} class="btn btn-warning my-2">Back</button>
      {/* <form> */}
<div class="form-row">
{/* <div class="form-group col-md-6">
<label for="inputEmail4">Email</label>
<input type="hidden" class="form-control" placeholder="Email" required value={customer.email} onChange={handleChange}/>
</div> */}
{/* <div class="form-group col-md-6">
<label for="inputPassword4">Password</label>
<input type="password" class="form-control" name ="password" id="inputPassword4" placeholder="Password" required value={vendor.password} onChange={handleChange}/>
</div> */}
</div> 

<div class="form-group">
<label for="inputAddress"><b>First Name</b></label>
<input type="text" class="form-control bg-transparent" name ="first_name" required value={customer.first_name} onChange={handleChange}/>
</div>
<div class="form-group">
<label for="inputAddress2"><b>Last Name</b></label>
<input type="text" class="form-control bg-transparent" name ="last_name" required value={customer.last_name} onChange={handleChange}/>
</div>
{/* <div class="form-group">
<label for="inputAddress2"><b>Company Name</b></label>
<input type="text" class="form-control bg-transparent" name ="cmp_name" required value={customer.cmp_name} onChange={handleChange}/>
</div> */}
<div class="form-group">
<label for="inputAddress2"><b>Mobile No.</b></label>
<input type="text" class="form-control bg-transparent" name ="mob_no"  pattern="^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$" required value={customer.mob_no} onChange={handleChange}/>
</div>
<div class="form-group">
<label for="inputAddress2"><b>Date Of Birth</b></label>
<input type="date" class="form-control bg-transparent" name ="dob" required value={customer.dob} onChange={handleChange}/>
</div>
<div class="form-row">
<div class="form-group ">
<label for="inputState"><b>Gender</b></label>
<select name="gender" required value={customer.gender} class="form-control bg-transparent" onChange={handleChange}>
                        <option value="">--Select Gender--</option>
                        <option value="MALE">Male</option>
                        <option value="FEMALE">Female</option>
                        <option value="OTHER">Other</option>
                    </select>
</div>
<div class="form-group ">
<label for="inputCity"><b>State</b></label>
<select name="state"  class="form-control bg-transparent" required onChange={getCity}>
                        <option value="">--Select State--</option>
                        {statesList.map((state)=>{
                            return(
                                <option value={state}>{state}</option> 
                            )   
                        })}
                    </select>
</div>
<div class="form-group ">
<label for="inputCity"><b>City</b></label>
<select name="city"  class="form-control bg-transparent" required onChange={getPincode}>
                        <option value="">--Select City--</option>
                        {cityList.map((city)=>{
                            return(
                                <option value={city}>{city}</option> 
                            )       
                        })}
                </select>
</div>
<div class="form-group ">
<label for="inputZip"><b>Pin Code</b></label>
<select name="pincode" required value={customer.pincode} class="form-control bg-transparent" onChange={handleChange}>
                        <option value="">--Select Pincode--</option>
                        {pincodes.map((pin)=>{
                            return(
                                <option value={pin}>{pin}</option> 
                            )       
                        })}
                </select>
</div>
</div>
<button type="button"  class="btn btn-primary my-3" onClick={updateProfile}>Update</button>
{/* </form> */}
</div>
</div>)
}

export default CustomerProfile;