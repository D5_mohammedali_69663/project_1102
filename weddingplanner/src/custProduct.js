import axios from "axios";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import '../node_modules/bootstrap/dist/css/bootstrap.css'

function CustomerProduct(props) {
    var i = 1;

    var history = useHistory();
    console.log(props.location.state.sid)

    var [product, setProduct] = useState({
        id: 0,
        servName: "",
        serv_desc: "",
        serv_price: 0,
        creatn_date: "",
        first_name: "",
        cmp_name: "",
        pincode: 0,
        city: "",
        state: "",
        mob_no: 0,
        email: ""
    });

    var [order, setOrder] = useState(
        {
            book_date: "",
            servName: "",
            cemail: "",
            vemail: ""
        }
    )

useEffect(() => {
    var url = "http://localhost:8080/Project/customer/service/" + props.location.state.sid;
    axios.get(url)
        .then((response) => {
            var result = response.data;
            setProduct(result);
        })
        .catch((err) => {
            console.log(err);
        })
}, [])

//go back function
var goBack = () => {
    history.goBack();
}

//handle change function
var handleChange=(args)=>{
    var copyOfOrders = {...order};
    copyOfOrders[args.target.name] = args.target.value;
    setOrder(copyOfOrders);
}

//createOrder function
var createOrder = (args) => {
    order.cemail = sessionStorage.getItem("email");
    order.vemail = product.email;
    order.servName = product.servName;

    var data = {...order};
    var url = "http://localhost:8080/Project/customer/p/createOrder/"+order.cemail;
    axios.post(url, data)
    .then((response)=>{
        if(response.data == true){
            debugger;
            alert("added order successfully");
            history.goBack();
            debugger;
        }else{
            alert(response.data);
        }
    })

}
var[chat, setChat] = useState({
    custEmail:"",
    vendEmail:"",
    message:"hi"
})
var createChat=()=>{
    var url = "http://localhost:8080/Project/customer/p/mail/"+sessionStorage.getItem("email");
    chat.custEmail = sessionStorage.getItem("email");
    chat.vendEmail = product.email;
    debugger;
    axios.post(url, chat)
    .then((resposne)=>{
        if(resposne.data){
            history.push("/customer/mail");
        }
    })
    .catch((err)=>{
        alert(err.message);
    })
}

var date = () => {
    <div>
        <input type="date" name="book_date"></input>
    </div>
}


return (<div className="row p-5 mb-1 mx-4">
    <h1><b><i>Customer Services</i></b></h1>
    <div style={{width: 600}}>
    <div className="col-4"><button className="col-4" class="btn btn-warning my-2" onClick={goBack}>back</button></div>
    
    <div id="ServiceDescription" classname="col-4">
        <table className="table-bordered col-4" class="table" striped hover >
        <thead class="thead-dark">
            
                <tr class="table-bg-transparent"> 
                    <td>Service Name</td><td>{product.servName}</td>
                </tr>
                <tr class="table-bg-transparent">
                    <td>Description</td><td>{product.serv_desc}</td>
                </tr>

                <tr class="table-bg-transparent">
                    <td>Serving Since</td><td>{product.creatn_date}</td>
                </tr>
                <tr class="table-bg-transparent">
                    <td>Name of Vendor</td><td>{product.first_name}</td></tr>
                <tr class="table-bg-transparent">
                    <td>Company Name</td> <td>{product.cmp_name}</td>
                </tr>
                <tr class="table-bg-transparent">
                    <td>City</td><td>{product.city}</td>
                </tr>
                <tr class="table-bg-transparent">
                    <td>State</td><td>{product.state}</td>
                </tr>
                <tr class="table-bg-transparent">
                    <td>Vendor Contact info</td><td>{product.mob_no}</td>
                </tr>
                <tr class="table-bg-transparent">
                    <td>Price per Unit</td><td>{product.serv_price}</td>
                </tr>
           
            </thead>
        </table>
    </div>
     {/* <div id="bookingDate" classname="col-4">
        <h4>Choose your date</h4>
        <input type="date" name="book_date" value={order.book_date} onChange={handleChange}></input>
        <button onClick={createOrder}>Create a Booking</button>
    </div>  */}
    <div class="form-group" id="bookingDate" classname="col-4">
<label for="inputAddress2">Choose your date</label>
<input type="date" class="form-control bg-transparent" name ="book_date" id="inputAddress2"  value={order.book_date} onChange={handleChange}/>
</div>
    <div className="col-8 my-1"><button class="btn btn-primary my-3" onClick={createOrder}>Create a Booking</button> </div>
    <div className="col-8 my-1"><button class="btn btn-primary my-3" onClick={createChat}>Talk to the vendor</button> </div>

    </div>
</div>)
}

export default CustomerProduct;