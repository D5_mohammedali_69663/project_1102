import { useState, useEffect } from "react";
import axios from 'axios';
import { useHistory } from "react-router-dom";
function CustReg(){

    var[customer, setCustomer] = useState({
        email: "",
        password: "",
        first_name: "",
        last_name: "",
        mob_no: "",
        dob: "",
        gender: "",
        pincode:0
    });

    var history = useHistory();

    //On Fail message
    var [message, setMessage] = useState({message:""})
    
    //array of list of states
    var[statesList, setStatesList] = useState([]);
    //array of list of cities
    var [cityList, setCityList] = useState([]);  
    // array of list of pincodes
    var [pincodes, setPincodes] = useState([]);


    //on mount getting list of states
    useEffect(()=>{
        //call to get list of states
        debugger;
        getState();
           
    },[]);

    var getState = ()=>{
        axios.get("http://localhost:8080/Project/aux/state")
             .then(function(response){
                var states = response.data;
                setStatesList(states);
                // debugger;
             })
    }
    //On state select getting citylist
    var getCity = (args)=>{
        var statesSelected = args.target.value;
        var cityUrl = "http://localhost:8080/Project/aux/city/" + statesSelected;
        // debugger;
        axios.get(cityUrl)
             .then(function(response){
                var cities = response.data;
                setCityList(cities);
                // debugger;
             }) 
    }

    //On change city list
    var getPincode = (args)=>{
        var citySelected = args.target.value;
        var pinUrl = "http://localhost:8080/Project/aux/pincode/" + citySelected;
        debugger;
        axios.get(pinUrl)
             .then(function(response){
                var pincode = response.data;
                setPincodes(pincode);
                debugger;
             }) 
    }



    //OnChange handler
    var handleChange=(args)=>{
        var copyOfCustomer = {...customer};
        copyOfCustomer[args.target.name] = args.target.value;
        setCustomer(copyOfCustomer);
    }
    var onRegister;
    //On submit Register Function
    var Register=()=>{
        debugger;
        var cData = {...customer};
        axios.post("http://localhost:8080/Project/register/customer",cData)
        .then((response)=>{
            onRegister = response.data;
        })

        if(onRegister!=null){
            history.push("/home");
        }else{
            setMessage({message:"Error regustering"});
        }
        
    }

    return(<div>
        <h1>Customer Registration Page</h1>
        {/* <form onSubmit={Register}> */}
        <table>
            <tbody>
                <tr>
                    <td>Email</td>
                    <td><input type="email" name="email" required value={customer.email} onChange={handleChange}></input></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="password" name="password" required value={customer.password} onChange={handleChange}></input></td>
                </tr>
                <tr>
                    <td>First Name</td>
                    <td><input type="text" name="first_name" required value={customer.first_name} onChange={handleChange}></input></td>
                </tr>
                <tr>
                    <td>Last Name</td>
                    <td><input type="text" name="last_name" required value={customer.last_name} onChange={handleChange}></input></td>
                </tr>
                <tr>
                    <td>Mobile no.</td>
                    <td><input type="text" name="mob_no" pattern="^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$" required value={customer.mob_no} onChange={handleChange}></input></td>
                </tr>
                <tr>
                    <td>Date of Birth</td>
                    <td><input type="date" name="dob" required value={customer.dob} onChange={handleChange}></input></td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td>
                        <select name="gender" required value={customer.gender} onChange={handleChange}>
                            <option value="">--Select Gender--</option>
                            <option value="MALE">Male</option>
                            <option value="FEMALE">Female</option>
                            <option value="OTHER">Other</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>State</td>
                    <td><select name="state" required  onChange={getCity}>
                            <option value="">--Select State--</option>
                            {statesList.map((state)=>{
                                return(
                                    <option value={state}>{state}</option> 
                                )   
                            })}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>City</td>
                    <td>
                    <select name="city" required value={customer.city} onChange={getPincode}>
                            <option value="">--Select City--</option>
                            {cityList.map((city)=>{
                                return(
                                    <option value={city}>{city}</option> 
                                )       
                            })}
                    </select>
                    </td>
                </tr>
                <tr>
                    <td>Pincode</td>
                    <td>
                    <select name="pincode" required value={customer.pincode} onChange={handleChange}>
                            <option value="">--Select Pincode--</option>
                            {pincodes.map((pin)=>{
                                return(
                                    <option value={pin}>{pin}</option> 
                                )       
                            })}
                    </select>
                    </td>
                </tr>   
                <tr>
                    <td colSpan="2"><button onClick={Register}>Register</button></td>
                </tr> 
                <div>{setMessage}</div>
            </tbody>
        </table>
        {/* </form> */}
    </div>
    )
}

export default CustReg;