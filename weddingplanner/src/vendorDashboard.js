import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

function VendorDashboard(){



    return(<div className="p-5 mb-1 mx-4">
        <h1><b><i>Vendor Dashboard</i></b></h1>
        <a href="vendor/services"><button class="btn btn-dark mx-4" >services</button></a> 
        <a href="vendor/orders" ><button class="btn btn-dark mx-4">orders</button></a> 
        <a href="vendor/profile" ><button class="btn btn-dark mx-4">Update Profile</button></a> 
        <a href="vendor/mail" ><button class="btn btn-dark mx-4">mail</button></a>
    </div>)

}

export default VendorDashboard;