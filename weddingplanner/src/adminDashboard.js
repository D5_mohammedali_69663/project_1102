import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { Table } from "react-bootstrap";

function AdminDashboard(){
    

    var history = useHistory();
    //add customer functions
    var addCust=()=>{
        debugger;
        history.push('/customerReg');
    }

    //add vendor
    var addVend=()=>{
        history.push('/vendorReg')
    }
    //view Vendors
    var viewCust=()=>{
        history.push('/admin/customers')
    }

    //view Customers
    var veiwVend=()=>{
        history.push('/admin/vendors');
    }
    //view Orders
    var veiwOrders=()=>{
        history.push('/admin/orders');
    }

    //view categores
    var veiwCategories=()=>{
        history.push('/admin/categories');
    }

    //view addresses
    var viewAddresses=()=>{
        history.push('/admin/addresses');
    }

    
    return(<div className="p-5 mb-1 mx-4">
        <h1><b><i>Admin Dashboard</i></b></h1>
             <Table   >
   
      <tbody>
        <tr>
        <td><button onClick={addCust} class="btn btn-dark">Add Customer</button></td>
        <td><button onClick={addVend} class="btn btn-dark">Add Vendor</button></td>
        <td><button onClick={viewCust} class="btn btn-dark">Customers</button></td>
        <td><button onClick={veiwVend} class="btn btn-dark">Vendors</button></td>
        <td><button onClick={veiwOrders} class="btn btn-dark">Orders</button></td> 
        <td> <button onClick={veiwCategories} class="btn btn-dark">Categories</button></td>
        <td><button onClick={viewAddresses} class="btn btn-dark">Addresses</button></td>
        
        </tr>
    </tbody>
    </Table>
    </div>)
}


export default AdminDashboard;

{/* <button type="button" class="btn btn-dark">Dark</button> */}
{/* <button onClick={addCust}>Add Customer</button> 
        <button onClick={addVend}>Add Vendor</button> 
        <button onClick={viewCust}>Customers</button> 
        <button onClick={veiwVend}>Vendors</button> */}




    //     <Table striped bordered hover>
   
    //   <tbody>
    //     <tr>
    //       
    //       <td><button onClick={addCust}>Add Customer</button></td>
    //       <td><button onClick={addVend}>Add Vendor</button></td>
    //       <td><button onClick={viewCust}>Customers</button></td>
    //       <td><button onClick={veiwVend}>Vendors</button></td>
    //     </tr>
    //</tbody>
    // </Table>
    //     <tr>
    //       <td>2</td>
    //       <td>Jacob</td>
    //       <td>Thornton</td>
    //       <td>@fat</td>
    //     </tr>
    //     <tr>
    //       <td>3</td>
    //       <td colSpan={2}>Larry the Bird</td>
    //       <td>@twitter</td>
    //     </tr>
    //   </tbody>
    // </Table>