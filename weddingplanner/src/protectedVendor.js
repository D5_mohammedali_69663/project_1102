import Home from './home';
import { Route } from "react-router-dom";

function ProtectedVendor(props){
    
    var isLoggedIn = sessionStorage.getItem("isLoggedIn");
    var role = sessionStorage.getItem("role");


   if(isLoggedIn && role == "ROLE_VENDOR") //check for sessionStorage values
//    if(true)
    {
        return <Route  path={props.path} exact   
                        component={props.component} />;
    }
    else
    {
       return <Home></Home>
    }
}

export default ProtectedVendor;