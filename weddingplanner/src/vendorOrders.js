import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

function VendorOrders() {
    //index
    var i=1;
    
    //error message
    var message="";
    var history = useHistory();
    var [orders, setOrders] = useState([{
        id:0,
        creat_date: "",
        book_date: "",
        book_status: "",
        order_status: "",
        servName: "",
        serv_price:0,
        cemail: ""
    }])

    //on page load bring in all orders
    useEffect(()=>{
        var url = "http://localhost:8080/Project/vendor/orders/" + sessionStorage.getItem("email");
        axios.get(url)
        .then((response)=>{
            var list = response.data;
            setOrders(list);
        })
        .catch((err)=>{
            console.log(err);
        })
    },[])

    //update order function
    var updateOrder=(args)=>{
        debugger;
        var url = "http://localhost:8080/Project/vendor/orders/"+args.target.value;
        axios.put(url)
        .then((response)=>{
            debugger;
            var result = response.data;
            debugger;
            if(result==true){
                window.location.reload();
                debugger;
            }else{
                alert(result);
                // setTimeout(()=>{message=""},3000);
                debugger;
            }
        })
    };

    //cancel order function
    var cancelOrder=(args)=>{
        var url = "http://localhost:8080/Project/vendor/orders/"+args.target.value;
        axios.delete(url)
        .then((resposne)=>{
            if(resposne.data){
                window.location.reload();
            }
        })
    }

    var goBack=()=>{
        history.goBack();
    }

    return (<div className="p-5 mb-1 mx-4">
        <h1><b><i>My Orders</i></b></h1>
        <button class="btn btn-dark mx-4" onClick={goBack}>Back</button>
        <div>{message}</div>
        <table class="table">
        <thead class="thead-dark">
        <tr class="table-primary">
        <th scope="col">Id</th>
        <th scope="col">Service Name</th>
        <th scope="col">Created On</th>
        <th scope="col">Booked On</th>
        <th scope="col">Booking Status</th>
        <th scope="col">Completion Status</th>
        <th scope="col">Price</th>
        <th scope="col">Customer Email</th>
            </tr>
            </thead>
            {orders.map((o) => {
                return (
                        <tbody>
                    <tr id={o.servName}>
                    <th class="table-bg-transparent" scope="row">{i++}</th>
                        
                        <td class="table-bg-transparent"><b>{o.servName}</b></td>
                        <td class="table-bg-transparent"><b>{o.creat_date}</b></td>
                        <td class="table-bg-transparent"><b>{o.book_date}</b></td>
                        <td class="table-bg-transparent"><b>{o.book_status}</b></td>
                        <td class="table-bg-transparent"><b>{o.order_status}</b></td>
                        <td class="table-bg-transparent"><b>{o.serv_price}</b></td>
                        <td class="table-bg-transparent"><b>{o.cemail}</b></td>
                        
                        {/* <td><button value={o.servName} onClick={deleteService}>delete</button></td> */}
                        <td><button class="btn btn-success  mx-1" onClick={updateOrder} value={o.id}>Mark Completed</button></td>
                        <td><button class="btn btn-danger  mx-1" onClick={cancelOrder} value={o.id}>Cancel</button></td> 
                    </tr>
                    </tbody>
                            
                    
                )
            })}
        </table>
        
    </div>)
}

export default VendorOrders;