import { useEffect, useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";

function CustomerDashboard() {
    var i = 1;

    var history = useHistory();

    //function to view Service, behave api call
    var viewService = (args) => {
        var behaveUrl = "http://localhost:8080/Project/customer/behaviour/" + sessionStorage.getItem("email") + "/" + args.target.value;
        axios.post(behaveUrl)
            .then((response) => {
                var res = response.data;
            })
            .catch((err) => {
                console.log(err);
            })
        var sid = args.target.value;
        history.push({
            pathname: "/customer/browse/product",
            state: { sid }

        });
    }

    useEffect(()=>{
        var url = "http://localhost:8080/Project/customer/p/recommend/"+sessionStorage.getItem("email");
        axios.get(url)
        .then((response)=>{
            var list  = response.data;
            setRecom(list);
        })
        .catch((err)=>{
            alert(err.message);
        })

    },[])

    var [recom, setRecom] = useState([{
        id: 0,
        servName: " ",
        serv_desc: "",
        serv_price: 0,
        creatn_date: "",
        first_name: "",
        cmp_name: "",
        pincode: 0,
        city: "",
        state: "",
        mob_no: 0,
        email: ""
    }])

    return (<div className="p-5 mb-1 mx-4">
        <h1><b><i>Customer Dashboard</i></b></h1>
        <a href="/customer/browse"><button class="btn btn-dark mx-4" >Browse Services</button></a>
        <a href="/customer/profile" ><button class="btn btn-dark mx-4">Update Profile</button></a>
        <a href="/customer/order" ><button class="btn btn-dark mx-4">Orders</button></a>
        <a href="/customer/mail" ><button class="btn btn-dark mx-4">Mail</button></a>

        <hr/>
        <h2>Recommendations for you</h2>

        <table class="table">
            <thead class="thead-dark">
                <tr class="table-primary">
                    <th scope="col">Sr No.</th>
                    <th scope="col">Service Name</th>
                    <th scope="col">Discription</th>
                    <th scope="col">City</th>
                    <th scope="col">State</th>
                    <th scope="col">Price Per Unit</th>
                </tr>
            </thead>
            {recom.map((o) => {
                return (
                    <tbody>
                        <tr id={o.servName}>
                            <th class="table-bg-transparent" scope="row">{i++}</th>
                            <td class="table-bg-transparent"><b>{o.servName}</b></td>
                            <td class="table-bg-transparent"><b>{o.serv_desc}</b></td>
                            <td class="table-bg-transparent"><b>{o.city}</b></td>
                            <td class="table-bg-transparent"><b>{o.state}</b></td>
                            <td class="table-bg-transparent"><b>{o.serv_price}</b></td>
                            <td><button onClick={viewService} class="btn btn-info" value={o.id}>View</button></td>
                        </tr>
                    </tbody>

                )
            })}

        </table>
    </div>)
}


export default CustomerDashboard;