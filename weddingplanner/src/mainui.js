import Home from './home';
import Footer from './footer';
import Menubars from './menubars'
import Header from './header';
import CustReg from './custregs';
import Login from './login';
import NotFound from './notfound';
import ProtectedRoute from './protectedRoute';
import ProtectedAdmin from './protectedAdmin';
import ProtectedVendor from './protectedVendor';
import Contact from './contact';
import Customers from './customers';
import Sidebar from './sidebar';
import Vendors from './vendors';
import VendorReg from './vendoreg';
import { Link, Route, Routes, Switch, useHistory } from 'react-router-dom';
import Feedback from 'react-bootstrap/esm/Feedback';
import { Col } from 'react-bootstrap';
import { Container, Row } from 'reactstrap';
import AdminDashboard from './adminDashboard';
import Addresses from './addresses';
import ViewPage from './viewpage';
import Orders from './orders';
import Categories from './categories';

import VendorDashboard from './vendorDashboard';
import VendorServices from './vendorServices';
import VendorOrders from './vendorOrders';
import VendorProfile from './vendorProfile';
import VendorMail from './vendorMail';

import d from './d8.jpg'


import ProtectedCustomer from './protectedCustomer'
import CustomerDashboard from './customerDashboard';
import CutstomerBrowse from './custBrowse';
import CustomerProduct from './custProduct';
import CustomerProfile from './customerProfile';
import CustomerOrders from './customerOrders';
import CustomerMail from './customerMail';

import AboutUs from './aboutUs';



function MainUI() {


  return (<div>

    <Header></Header>
    <Row>
      {/* <Col md={2}><Sidebar /> 
      </Col>*/}
      <Col md={12}><Menubars />
      
      <Row>
        <div className=" mb-1'container-fluid'" style={{background: `url(${d})`, zoom: "103%"}}>
    <Switch>
      {/* <Route path='/' exact component={Home} /> */}
      {/* <Route path='/home' exact component={Home} /> */}
      <Route path='/home' exact component={ViewPage} />
      

      <Route path='/customerReg' exact component={CustReg}></Route>
      <Route path='/vendorReg' exact component={VendorReg}></Route>
      <Route path='/login' exact component={Login}/>
      <Route path='/feedback' exact component={Feedback}/>
      <Route path='/' exact component={ViewPage}/>
      <Route path='/home' exact component={ViewPage}/>
      <Route path='/contact' exact component={Contact}/>
      <Route path='/aboutus' exact component={AboutUs}/>
      {/* <ProtectedAdmin path='/customers' exact component={Customers}/>
      <ProtectedAdmin path='/vendors' exact component={Vendors}/>
      <ProtectedAdmin path='/admin' exact component={AdminDashboard}/>
      <ProtectedRoute path='/contact' exact component={Contact} />
      <ProtectedRoute path='/addresses' exact component={Addresses} /> */}


       <ProtectedAdmin path='/admin/categories' exact component={Categories}/>
      <ProtectedAdmin path='/admin/customers' exact component={Customers}/>
      <ProtectedAdmin path='/admin/orders' exact component={Orders}/>
      <ProtectedAdmin path='/admin/vendors' exact component={Vendors}/>
      <ProtectedAdmin path='/admin/addresses' exact component={Addresses}/>
      <ProtectedAdmin path='/admin' exact component={AdminDashboard}/>

      <ProtectedVendor path='/vendor' exact component={VendorDashboard}/>
      <ProtectedVendor path='/vendor/services' exact component={VendorServices}/>
      <ProtectedVendor path='/vendor/orders' exact component={VendorOrders}/>
      <ProtectedVendor path='/vendor/profile' exact component={VendorProfile}/>
      <ProtectedVendor path='/vendor/mail' exact component={VendorMail}/>


      <ProtectedCustomer path='/customer' exact component={CustomerDashboard}/>
      <ProtectedCustomer path='/customer/browse' exact component={CutstomerBrowse}/>
      <ProtectedCustomer path='/customer/browse/product' exact component={CustomerProduct}/>
      <ProtectedCustomer path='/customer/profile' exact component={CustomerProfile}/>
      <ProtectedCustomer path='/customer/order' exact component={CustomerOrders}/>
      <ProtectedCustomer path='/customer/mail' exact component={CustomerMail}/>





      <ProtectedRoute path='/contact' exact component={Contact} />


      <Route path='*' component={NotFound} />
    </Switch>
    </div>
    </Row>
    </Col>
    </Row>
    <Footer />
  </div>)

}

export default MainUI;