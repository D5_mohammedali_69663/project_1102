import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import Button from 'react-bootstrap/Button';
import {MDBBtn, MDBRow, MDBContainer, MDBCol, MDBInput} from  'mdb-ui-kit'
import img from './fotor1.png';
import { useHistory } from 'react-router-dom';
import d from './ss.jpg'
function Header() {

  var history = useHistory();

  var SignIn =()=>{
    history.push("/login")
  }

  var logOut=()=>{
    sessionStorage.clear();
    history.push("/home");
  }


  return (<div className='container-fluid' style={{background: `url(${d})`, zoom: "80%"}}>
    <Navbar className="navbar-inverse">
      <Container>
        {/* <img height={150} width={200}
          src="https://img.freepik.com/free-vector/just-married-couple-motorcycle-avatars-characters_24877-50488.jpg?w=740&t=st=1677849391~exp=1677849991~hmac=da026ca8f0d242ee9f62aa9cfa844e6a99d500343798923246c01437ce84110a"
          rounded
        /> */}
        <div className="align-self-auto"> <img className=".float-start" src={img} height={240} width={280}/></div>
       
        {/* <div className='text-center'>Weeding Planner</div> */}
        <Navbar.Brand href="#home" className="my-3"><font color="#ABF9EE"><h1><b>Shaadi Mubarak</b></h1>
        </font></Navbar.Brand>
        {/* <Nav className="me-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#features">Features</Nav.Link>
            <Nav.Link href="#pricing">Pricing</Nav.Link>
            <Nav.Link href="#feedback">Feedback</Nav.Link>
            <div className="position-absolute top-0 end-0">
           */}
        <div className='justify-content-right mr-2 '>
        <button type="button" class="btn btn-success" style={{marginRight:20}} onClick={SignIn}> 
        {/* <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" fill="currentColor" class="bi bi-lock" viewBox="0 0 16 16">
  <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2zM5 8h6a1 1 0 0 1 1 1v5a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1z"/>
</svg> */}
Sign In
</button>
        <button type="button" class="btn btn-danger" style={{marginRight:20}} onClick={logOut}> 
        {/* <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" fill="currentColor" class="bi bi-lock-fill" viewBox="0 0 16 16">
  <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"/>
</svg> */}
Logout
</button>
          
           
        </div>
      </Container>
    </Navbar>
  </div>)
}

export default Header;