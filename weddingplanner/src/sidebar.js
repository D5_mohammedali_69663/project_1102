import React from 'react';
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from 'cdbreact';
import Button from 'react-bootstrap/Button';
import d from './4k.png'
function Sidebar(){


    return (<div style={{ display: 'flow', height: '175vh', overflow: 'scroll initial',background: `url(${d})`, zoom: "100%"}}>
    <CDBSidebar textColor="#fff">
      <CDBSidebarHeader>
        <a href="/" className="text-decoration-none" style={{ color: 'inherit' }}>
        <b><i>Shaadi Mubarak</i></b>
        </a>
      </CDBSidebarHeader>

      <CDBSidebarFooter style={{ textAlign: 'center' }}>
        <div
          className="sidebar-btn-wrapper"
          style={{
            padding: '20px 5px',
          }}
        >
          <Button variant="light" size="md" className='my-2'>
          Profile  
      </Button>
      <br></br>
      <Button variant="light" size="md" className='my-2'>
      Categories   
      </Button>
      <br></br>
      {/* <Button variant="light" size="md" className='my-2'>
      Music
      </Button>
      <br></br>
      <Button variant="light" size="md" className='my-2'>
        Mehendi
      </Button>
      <br></br>
      <Button variant="light" size="md" className='my-2'>
        Food
      </Button>
      <br></br>
      <Button variant="light" size="md" className='my-2'>
        Decorations
      </Button> */}
        </div>
      </CDBSidebarFooter>
    </CDBSidebar>
  </div>
);
};

export default Sidebar;