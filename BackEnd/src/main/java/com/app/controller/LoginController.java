package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.UserDto;
import com.app.service.IUserService;


@CrossOrigin
@RestController
@RequestMapping("/Login")
public class LoginController {
	
	@Autowired
	IUserService userService;
	
	@PostMapping
	public UserDto authorizeUser(@RequestBody UserDto userdto) {
		UserDto authUser = userService.authUser(userdto);
		
		return authUser;
	}
}
