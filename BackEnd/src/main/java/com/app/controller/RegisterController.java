package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.CustomerDto;
import com.app.dto.VendorDto;
import com.app.service.ICustomerService;
import com.app.service.IVendorService;


@CrossOrigin
@RestController
@RequestMapping("/register")
public class RegisterController {
	
	@Autowired
	IVendorService vendorService;
	
	@Autowired
	ICustomerService customerService;
	
	@PostMapping("/customer")
	public String addCustomer(@RequestBody CustomerDto custdto) {
		String mesg = customerService.addCustomer(custdto);	
		return mesg;
	}
	
	@PostMapping("/vendor")
	public String addVendor(@RequestBody VendorDto vendordto) {
		String mesg = vendorService.addVendor(vendordto);
		
		return mesg;
	}
	
}
