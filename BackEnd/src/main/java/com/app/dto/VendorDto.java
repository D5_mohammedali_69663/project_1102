package com.app.dto;

import java.time.LocalDate;

import com.app.model.Address;
import com.app.model.Gender;
import com.app.model.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@JsonInclude(content = Include.NON_NULL, value = Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VendorDto {
	private String email;
<<<<<<< HEAD
//	@JsonIgnore
=======

>>>>>>> ff3bc339f1db361c56bd9c5ffdc511a3ff7471a6
	private String password;
	private String first_name;
	private String last_name;
	private Long mob_no;
	private LocalDate dob;
	private Gender gender;
	private Address address;
	private Integer pincode;
	private Role role = Role.ROLE_VENDOR;
	private String cmp_name;
	public VendorDto(String email, String password, String first_name, String last_name, Long mob_no, LocalDate dob,
			Gender gender, Integer pincode, String cmp_name) {
		super();
		this.email = email;
		this.password = password;
		this.first_name = first_name;
		this.last_name = last_name;
		this.mob_no = mob_no;
		this.dob = dob;
		this.gender = gender;
		this.pincode = pincode;
		this.cmp_name = cmp_name;
	}
	public VendorDto(String password, String first_name, String last_name, Long mob_no, LocalDate dob, Gender gender,
			Integer pincode, String cmp_name) {
		super();
		this.password = password;
		this.first_name = first_name;
		this.last_name = last_name;
		this.mob_no = mob_no;
		this.dob = dob;
		this.gender = gender;
		this.pincode = pincode;
		this.cmp_name = cmp_name;
	}
	
	
	
	
}
