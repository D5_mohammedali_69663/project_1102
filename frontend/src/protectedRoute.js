import Login from './login';
import { Route } from "react-router-dom";

function ProtectedRoute(props){
    
    var isLoggedIn = false;

    if(isLoggedIn) //check for sessionStorage values
    {
        return <Route  path={props.path} exact   
                        component={props.component} />;
    }
    else
    {
       return <Login></Login>
    }
}

export default ProtectedRoute;