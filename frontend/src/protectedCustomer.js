import Home from './home';
import { Route } from "react-router-dom";

function ProtectedRoute(props){
    
    var isLoggedIn = sessionStorage.getItem("isLoggedIn");
    var role = sessionStorage.getItem("role");

    if(isLoggedIn && role=="CUSTOMER") //check for sessionStorage values
    {
        return <Route  path={props.path} exact   
                        component={props.component} />;
    }
    else
    {
       return <Home></Home>
    }
}

export default ProtectedRoute;