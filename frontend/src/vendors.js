import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
function Vendors(){

    //index param
    var i = 1;
    var[vendorList, setVendorList] = useState([{
        email: "abhi@a.com",
        first_name: "Abhinandan",
        last_name: "Singh",
        mob_no: 7722037553,
        dob: "1999-10-24",
        gender: "MALE",
        address: {
            pincode: 411045,
            city: "Pune",
            state: "Maharashtra"
        },
        pincode: 411045,
        role: "ROLE_VENDOR",
        cmp_name: "Badmash Company"
    }]);

    //history
    var history = useHistory(); 

    //Making api call on load
    useEffect(()=>{
        axios.get("http://localhost:8080/Project/admin/vendors")
        .then((response)=>{
            var list = response.data;
            setVendorList(list);
        }).catch((err)=>{
            console.log(err);
        })
    },[])

    var deleteVendor=(args)=>{
        var url = "http://localhost:8080/Project/admin/deleteVendor/"+args.target.value;
        axios.delete(url)
        .then((response)=>{
            console.log(response.data);
        })
        .catch((err)=>{
            console.log(err);
        })
        window.location.reload();
    }



    return(<div className="p-5">
        <h1>List of Vendors</h1>
        
        <table class="table">
  <thead class="thead-dark">
    <tr class="table-primary">
      <th scope="col">Id</th>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th>
      <th scope="col">Email</th>
      <th scope="col">Date Of Birth</th>
      <th scope="col">Gender</th>
      <th scope="col">Mobile</th>
      <th scope="col">Company Name</th>
      <th scope="col">PinCode</th>
      <th scope="col">City</th>
      <th scope="col">State</th>
    </tr>
  </thead>
  {vendorList.map((vendor)=>{
    return(<tbody>
        <tr>
        <th class="table-secondary" scope="row">{i++}</th>
        <td class="table-danger">{vendor.first_name}</td>
        <td class="table-danger">{vendor.last_name}</td>
        <td class="table-danger">{vendor.email}</td>
        <td class="table-danger">{vendor.dob}</td>
        <td class="table-danger">{vendor.gender}</td>
        <td class="table-danger">{vendor.mob_no}</td>
        <td class="table-danger">{vendor.cmp_name}</td>
        <td class="table-danger">{vendor.pincode}</td>
        <td class="table-danger">{vendor.city}</td>
        <td class="table-danger">{vendor.address.state}</td>
      </tr>
    </tbody>)
     })}
  </table>
  
      </div>)
 
    }
export default Vendors;