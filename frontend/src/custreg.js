import { useState, useEffect,  } from "react";
import axios from 'axios';
import { useHistory } from "react-router-dom";
function CustReg(){

    var[customer, setCustomer] = useState({
        email: "",
        password: "",
        first_name: "",
        last_name: "",
        mob_no: "",
        dob: "",
        gender: "",
        pincode:0
    });

    var history = useHistory();

    //On Fail message
    var [message, setMessage] = useState({message:""})
    
    //array of list of states
    var[statesList, setStatesList] = useState([]);
    //array of list of cities
    var [cityList, setCityList] = useState([]);  
    // array of list of pincodes
    var [pincodes, setPincodes] = useState([]);


    //on mount getting list of states
    useEffect(()=>{
        //call to get list of states
        debugger;
        getState();
           
    },[]);

    var getState = ()=>{
        axios.get("http://localhost:8080/Project/aux/state")
             .then(function(response){
                var states = response.data;
                setStatesList(states);
                // debugger;
             })
    }
    //On state select getting citylist
    var getCity = (args)=>{
        var statesSelected = args.target.value;
        var cityUrl = "http://localhost:8080/Project/aux/city/" + statesSelected;
        // debugger;
        axios.get(cityUrl)
             .then(function(response){
                var cities = response.data;
                setCityList(cities);
                // debugger;
             }) 
    }

    //On change city list
    var getPincode = (args)=>{
        var citySelected = args.target.value;
        var pinUrl = "http://localhost:8080/Project/aux/pincode/" + citySelected;
        debugger;
        axios.get(pinUrl)
             .then(function(response){
                var pincode = response.data;
                setPincodes(pincode);
                debugger;
             }) 
    }



    //OnChange handler
    var handleChange=(args)=>{
        var copyOfCustomer = {...customer};
        copyOfCustomer[args.target.name] = args.target.value;
        setCustomer(copyOfCustomer);
    }
    var onRegister;
    //On submit Register Function
    var Register=()=>{
        debugger;
        var cData = {...customer};
        axios.post("http://localhost:8080/Project/register/customer",cData)
        .then((response)=>{
            onRegister = response.data;
        })

        if(onRegister!=null){
            history.push("/home");
        }else{
            setMessage({message:"Error regustering"});
        }
        
    }

    return(<div className="p-5 mb-3 mx-4">
        <h1>Customer Registration Page</h1>
        <form>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Email</label>
      <input type="email" class="form-control" id="inputEmail4" placeholder="Email"/>
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Password</label>
      <input type="password" class="form-control" id="inputPassword4" placeholder="Password"/>
    </div>
  </div>
  <div class="form-group">
    <label for="inputAddress">Address</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St"/>
  </div>
  <div class="form-group">
    <label for="inputAddress2">Address 2</label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor"/>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputCity">City</label>
      <input type="text" class="form-control" id="inputCity"/>
    </div>
    <div class="form-group col-md-4">
      <label for="inputState">State</label>
      <select id="inputState" class="form-control">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-md-2">
      <label for="inputZip">Zip</label>
      <input type="text" class="form-control" id="inputZip"/>
    </div>
  </div>
  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="gridCheck"/>
      <label class="form-check-label" for="gridCheck">
        Check me out
      </label>
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Sign in</button>
</form>
    </div>
    )
}

export default CustReg;