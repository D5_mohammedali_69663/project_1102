import { Container } from 'react-bootstrap';
import Col from 'react-bootstrap/esm/Col';
import Nav from 'react-bootstrap/Nav';
import { Row } from 'reactstrap';
function Menubars(){
    return (<div>
     <header className='ml-2'>
  <nav class="navbar navbar-expand-md navbar-dark" style={{backgroundColor:"#2B2B3A"}}>
    
    <button
      class="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse " id="navbarSupportedContent">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="#">
            <i class="fa fa-home"></i>
            Home
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">
            <i class="fas fa-feather"></i>
            Features
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="#">
            <i class="fa fa-dollar"></i>
            Pricing
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">
            <i class="fa fa-cogs"></i>
            Option
          </a>
        </li>
      </ul>
    </div>
  </nav>
</header>

    </div>)
}


export default Menubars;