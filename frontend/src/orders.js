import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
function Orders(){
    var [orders, setOrders] = useState([{
        book_date: "",
        book_status: "",
        order_status: "",
        servName: "",
        serv_price: 0,
        cemail: "",
        vemail: ""
    }]);

    //index 
    var i=1;

    //on page load bring in all orders
    useEffect(()=>{
        axios.get("http://localhost:8080/Project/admin/orders")
        .then((response)=>{
            var list = response.data;
            setOrders(list);
        })
        .catch((err)=>{
            console.log(err);
        })
    },[])


    return(<div>
        <h1>All orders</h1>
        <table class="table">
  <thead class="thead-dark">
    <tr class="table-primary">
      <th scope="col">Id</th>
      <th scope="col">Booking date</th>
      <th scope="col">Confirmation Status</th>
      <th scope="col">Completion Status</th>
      <th scope="col">Service Name</th> 
      <th scope="col">Price</th>
      <th scope="col">Customer Email</th>
      <th scope="col">Vendor Email</th>
    </tr>
  </thead>
  {orders.map((o)=>{
    return(<tbody>
        <tr id={o.email}>
        <th class="table-secondary" scope="row">{i++}</th>
        <td class="table-danger">{o.book_date}</td>
        <td class="table-danger">{o.book_status}</td>
        <td class="table-danger">{o.order_status}</td>
        <td class="table-danger">{o.servName}</td>
        <td class="table-danger">{o.serv_price}</td>
        <td class="table-danger">{o.cemail}</td>
        <td class="table-danger">{o.vemail}</td>
      </tr>
    </tbody>)
     })}
  </table>
  
    </div>)
}

export default Orders;


// <table>
//             <tr>
//                 <td>Id</td>
//                 <td>Booking date</td>
//                 <td>Confirmation Status</td>
//                 <td>Completion Status</td>
//                 <td>Service Name</td>
//                 <td>Price</td>
//                 <td>Customer Email</td>
//                 <td>Vendor Email</td>
//             </tr>
//             {orders.map((o)=>{
//                 return(
//                     <tr id={o.email}>
//                 <td>{i++}</td>
//                 <td>{o.book_date}</td>
//                 <td>{o.book_status}</td>
//                 <td>{o.order_status}</td>
//                 <td>{o.servName}</td>
//                 <td>{o.serv_price}</td>
//                 <td>{o.cemail}</td>
//                 <td>{o.vemail}</td>
//             </tr>
//                 )
//             })}
//         </table>