import { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from 'axios';
import jwt_decode from "jwt-decode";

function Login(){
    //user credentials
    var[usercred, setUsercred] = useState({email:"", password:""});
    
    var history = useHistory();

    //input text handlechange function
    var handleChange=(args)=>{
        var copyOfusercred = {...usercred};
        copyOfusercred[args.target.name] = args.target.value;
        setUsercred(copyOfusercred);
    }
    var jwt;

    var Authenticate=()=>{
        debugger;
        var data = {...usercred};

        axios.post("http://localhost:8080/Project/authenticate", data)
        .then(function (response) {
            debugger;
            jwt = response.data;
            if(jwt != null){
                
                var decoded = jwt_decode(jwt);
                var email = decoded.email;
                var role = decoded.role[0].authority;
                sessionStorage.setItem("email", email);
                sessionStorage.setItem("role", role);
                sessionStorage.setItem("Authorization", jwt);
                debugger;
                console.log(decoded);
                history.push('/home');
            }

          })
          .catch(function (error) {
            //debugger;
            console.log(error);
          });

        // var copyOfusercred = {...usercred};
        // copyOfusercred={email:"", password:""};
        // setUsercred(copyOfusercred);
    }




    return(<div className="p-5">
        <h1>Login Page</h1>
        {/* <form onSubmit={Authenticate}>   */}
        <form>
  
  <div class="form-outline mb-4">
    <input type="email" id="form2Example1" class="form-control" value={usercred.email} required onChange={handleChange}/>
    <label class="form-label" for="form2Example1">Email Address</label>
  </div>

 
  <div class="form-outline mb-4">
    <input type="password" id="form2Example2" class="form-control" value={usercred.password} required onChange={handleChange}/>
    <label class="form-label" for="form2Example2">Password</label>
  </div>

  
  <div class="row mb-4">
    <div class="col d-flex justify-content-center">
      
      <div class="form-check">
        <input class="form-check-input" type="checkbox" value="" id="form2Example31" checked />
        <label class="form-check-label" for="form2Example31"> Remember me </label>
      </div>
    </div>

    {/* <div class="col">
      
      <a href="#!">Forgot password?</a>
    </div> */}
  </div>

  
  <button type="submit" class="btn btn-primary btn-block mb-4" onClick={Authenticate}>Sign in</button>

  
  <div class="text-center">
    <p>Not a member? <a href="#!">Customer Register</a></p>
    <p>Not a member? <a href="#!">Vendor Register</a></p>
    <p>or sign up with:</p>
    <button type="button" class="btn btn-link btn-floating mx-1">
      <i class="fab fa-facebook-f"></i>
    </button>

    <button type="button" class="btn btn-link btn-floating mx-1">
      <i class="fab fa-google"></i>
    </button>

    <button type="button" class="btn btn-link btn-floating mx-1">
      <i class="fab fa-twitter"></i>
    </button>

    <button type="button" class="btn btn-link btn-floating mx-1">
      <i class="fab fa-github"></i>
    </button>
  </div>
</form>
        {/* </form> */}
    </div>)
}

export default Login;