import Home from './home';
import Footer from './footer';
import Menubars from './menubars'
import Header from './header';
import CustReg from './custregs';
import Login from './login';
import NotFound from './notfound';
import ProtectedRoute from './protectedRoute';
import ProtectedAdmin from './protectedAdmin';
import Contact from './contact';
import Customers from './customers';
import Sidebar from './sidebar';
import Vendors from './vendors';
import VendorReg from './vendoreg';
import { Link, Route, Routes, Switch, useHistory } from 'react-router-dom';
import Feedback from 'react-bootstrap/esm/Feedback';
import { Col } from 'react-bootstrap';
import { Container, Row } from 'reactstrap';
import AdminDashboard from './adminDashboard';


function MainUI() {


  return (<div>

    <Header />
    <Row>
      <Col md={2}><Sidebar />
      </Col>
      <Col  style={{backgroundColor : "#A8D8CD"}} md={10}><Menubars />
      
      <Row>
            <Switch>
      <Route path='/' exact component={Home} />
      <Route path='/home' exact component={Home} />

      <Route path='/customerReg' exact component={CustReg}></Route>
      <Route path='/vendorReg' exact component={VendorReg}></Route>
      <Route path='/login' exact component={Login}/>
      <Route path='/feedback' exact component={Feedback}/>

      <ProtectedAdmin path='/customers' exact component={Customers}/>
      <ProtectedAdmin path='/vendors' exact component={Vendors}/>
      <ProtectedAdmin path='/admin' exact component={AdminDashboard}/>
      <ProtectedRoute path='/contact' exact component={Contact} />


      <Route path='*' component={NotFound} />
    </Switch>
    </Row>
    </Col>
    </Row>

    
    

    <Footer />
  </div>)

}

export default MainUI;