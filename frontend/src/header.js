import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import Button from 'react-bootstrap/Button';
import {MDBBtn, MDBRow, MDBContainer, MDBCol, MDBInput} from  'mdb-ui-kit'
function Header() {


  return (<div>


    <Navbar style={{background:' #FB7B8E'}}>
      <Container>
        <img height={150} width={200}
          src="https://img.freepik.com/free-vector/just-married-couple-motorcycle-avatars-characters_24877-50488.jpg?w=740&t=st=1677849391~exp=1677849991~hmac=da026ca8f0d242ee9f62aa9cfa844e6a99d500343798923246c01437ce84110a"
          rounded
        />
        {/* <div className='text-center'>Weeding Planner</div> */}
        <Navbar.Brand href="#home" className="my-3">Wedding Planner</Navbar.Brand>
        {/* <Nav className="me-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#features">Features</Nav.Link>
            <Nav.Link href="#pricing">Pricing</Nav.Link>
            <Nav.Link href="#feedback">Feedback</Nav.Link>
            <div className="position-absolute top-0 end-0">
           */}
        <div className='justify-content-right'>
        <button type="button" class="btn btn-success" style={{marginRight:32}}> <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" fill="currentColor" class="bi bi-file-earmark-check" viewBox="0 0 16 16">
  <path d="M10.854 7.854a.5.5 0 0 0-.708-.708L7.5 9.793 6.354 8.646a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z"/>
  <path d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2zM9.5 3A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5v2z"/>
</svg>Sign Up</button>
        <button type="button" class="btn btn-danger" style={{marginRight:32}}> <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" fill="currentColor" class="bi bi-file-earmark-excel" viewBox="0 0 16 16">
  <path d="M5.884 6.68a.5.5 0 1 0-.768.64L7.349 10l-2.233 2.68a.5.5 0 0 0 .768.64L8 10.781l2.116 2.54a.5.5 0 0 0 .768-.641L8.651 10l2.233-2.68a.5.5 0 0 0-.768-.64L8 9.219l-2.116-2.54z"/>
  <path d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2zM9.5 3A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5v2z"/>
</svg>Logout</button>
          
           
        </div>
      </Container>
    </Navbar>
  </div>)
}

export default Header;