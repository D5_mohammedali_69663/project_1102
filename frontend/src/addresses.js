import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";


function Addresses(){

    var [address, setAddress] = useState([]);

    useEffect(()=>{
        axios.get("http://localhost:8080/Project/admin/address")
        .then((response)=>{
            var list = response.data;
            setAddress(list);
        })
        .catch((err)=>{
            console.log(err);
        })
    },[])

    //serial
    var i=1;

    var[addrs, setAddrs] = useState({pincode:0, city:"", state:""});

    //OnChange handler
    var handleChange=(args)=>{
        var copyOfAddrs = {...addrs};
        copyOfAddrs[args.target.name] = args.target.value;
        setAddrs(copyOfAddrs);
    }

    //adding address
    var addAddress=(args)=>{
        var data = {...addrs};
        axios.post("http://localhost:8080/Project/admin/address", data)
        .then((response)=>{
            // debugger;
            var res = response.data;
        })
        .catch((err)=>{
            console.log(err);
        })
        window.location.reload();
    }

    return(<div>
        <h1> Address </h1>
        <table class="table">
        <tbody>
  <thead class="thead-dark">
    <tr class="table-primary">
      <th scope="col">Serial no.</th>
      <th scope="col">Pincode</th>
      <th scope="col">City</th>
      <th scope="col">State</th>
    </tr>
  </thead>
  {address.map((c)=>{
    return(
        <tr>
        <th class="table-secondary" scope="row">{i++}</th>
        <td class="table-danger">{c.pincode}</td>
        <td class="table-danger">{c.city}</td>
        <td class="table-danger">{c.state}</td>
      </tr>
      )
     })}
    
     <tr>
                    <td>New Address</td>
                    <td class="table-danger"><input type="number"  value={addrs.pincode} name="pincode" onChange={handleChange} ></input></td>
                    <td class="table-danger"><input type="text"  value={addrs.city} name="city" onChange={handleChange} ></input></td>
                    <td class="table-danger"><input type="text"  value={addrs.state} name="state" onChange={handleChange} ></input></td>
                    <td class="table-danger"><button onClick={addAddress} type="button" class="btn btn-dark">Add</button></td>
                </tr>
                </tbody>
  </table>
    </div>)



}

export default Addresses;


{/* <table>
            <tbody>
                <tr>
                    <td>Serial no.</td>
                    <td>Pincode</td>
                    <td>City</td>
                    <td>State</td>
                </tr>
                {address.map((c)=>{
                    return(<tr>
                        <td>{i++}</td>
                        <td>{c.pincode}</td>
                        <td>{c.city}</td>
                        <td>{c.state}</td>
                    </tr>)
                })}
                <tr>
                    <td>New Address</td>
                    <td><input type="number"  value={addrs.pincode} name="pincode" onChange={handleChange} ></input></td>
                    <td><input type="text"  value={addrs.city} name="city" onChange={handleChange} ></input></td>
                    <td><input type="text"  value={addrs.state} name="state" onChange={handleChange} ></input></td>
                    <td><button onClick={addAddress}>Add</button></td>
                </tr>
            </tbody>
        </table> */}