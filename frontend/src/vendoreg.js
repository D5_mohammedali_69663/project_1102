import { useState, useEffect } from "react";
import axios from 'axios';
import { useHistory } from "react-router-dom";

import state_city from './resources/state_city.json'

function VendorReg(){

    // history
    var history = useHistory();


    var[vendor, setVendor] = useState({
        email: "",
        password: "",
        first_name: "",
        last_name: "",
        mob_no: "",
        dob: "",
        gender: "",
        pincode:431006,
        cmp_name:""
    });


     //On Fail message
     var [message, setMessage] = useState();
    
     //array of list of states
     var[statesList, setStatesList] = useState([]);
     //array of list of cities
     var [cityList, setCityList] = useState([]);  
     // array of list of pincodes
     var [pincodes, setPincodes] = useState([]);    



 


    //OnChange handler
    var handleChange=(args)=>{
        var copyOfvendor = {...vendor};
        copyOfvendor[args.target.name] = args.target.value;
        setVendor(copyOfvendor);
    }





    //on mount getting list of states
    useEffect(()=>{
        //call to get list of states
        debugger;
        getState();
           
    },[]);

    var getState = ()=>{
        axios.get("http://localhost:8080/Project/aux/state")
             .then(function(response){
                var states = response.data;
                setStatesList(states);
                // debugger;
             })
    }
    //On state select getting citylist
    var getCity = (args)=>{
        var statesSelected = args.target.value;
        var cityUrl = "http://localhost:8080/Project/aux/city/" + statesSelected;
        // debugger;
        axios.get(cityUrl)
             .then(function(response){
                var cities = response.data;
                setCityList(cities);
                // debugger;
             }) 
    }

    //On change city list
    var getPincode = (args)=>{
        var citySelected = args.target.value;
        var pinUrl = "http://localhost:8080/Project/aux/pincode/" + citySelected;
        // debugger;
        axios.get(pinUrl)
             .then(function(response){
                var pincode = response.data;
                setPincodes(pincode);
                // debugger;
             }) 
    }



    var onRegister;


    //On submit Register Function
    var Register=()=>{
        debugger;
        var cData = {...vendor};
        axios.post("http://localhost:8080/Project/register/vendor",cData)
        .then((response)=>{
            onRegister = response.data;
            debugger;
        }).catch((err)=>{
            console.log(err);
        })

        if(onRegister!=null && onRegister == "true"){
            history.push("/home");  
            
        }else{            
            setMessage(onRegister);
            debugger;
        }
    }

    return(<div className="p-4 mb-3 mx-5">
    <h1>Vendor Registration Page</h1>
    <form>
<div class="form-row">
<div class="form-group col-md-6">
  <label for="inputEmail4">Email</label>
  <input type="email" class="form-control" id="inputEmail4" placeholder="Email" required value={vendor.email} onChange={handleChange}/>
</div>
<div class="form-group col-md-6">
  <label for="inputPassword4">Password</label>
  <input type="password" class="form-control" id="inputPassword4" placeholder="Password" required value={vendor.password} onChange={handleChange}/>
</div>
</div>
<div class="form-group">
<label for="inputAddress">First Name</label>
<input type="text" class="form-control" id="inputAddress" required value={vendor.first_name} onChange={handleChange}/>
</div>
<div class="form-group">
<label for="inputAddress2">Last Name</label>
<input type="text" class="form-control" id="inputAddress2" required value={vendor.last_name} onChange={handleChange}/>
</div>
<div class="form-group">
<label for="inputAddress2">Company Name</label>
<input type="text" class="form-control" id="inputAddress2" required value={vendor.cmp_name} onChange={handleChange}/>
</div>
<div class="form-group">
<label for="inputAddress2">Mobile No.</label>
<input type="text" class="form-control" id="inputAddress2" pattern="^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$" required value={vendor.mob_no} onChange={handleChange}/>
</div>
<div class="form-group">
<label for="inputAddress2">Date Of Birth</label>
<input type="date" class="form-control" id="inputAddress2" required value={vendor.dob} onChange={handleChange}/>
</div>
<div class="form-row">
<div class="form-group col-md-4">
  <label for="inputState">Gender</label>
  <select name="gender" required value={vendor.gender} class="form-control" onChange={handleChange}>
                            <option value="">--Select Gender--</option>
                            <option value="MALE">Male</option>
                            <option value="FEMALE">Female</option>
                            <option value="OTHER">Other</option>
                        </select>
</div>
<div class="form-group col-md-6">
  <label for="inputCity">State</label>
  <select name="state" required class="form-control" onChange={getCity}>
                            <option value="">--Select State--</option>
                            {statesList.map((state)=>{
                                return(
                                    <option value={state}>{state}</option> 
                                )   
                            })}
                        </select>
</div>
<div class="form-group col-md-6">
  <label for="inputCity">City</label>
  <select name="city" required value={vendor.city} class="form-control" onChange={getPincode}>
                            <option value="">--Select City--</option>
                            {cityList.map((city)=>{
                                return(
                                    <option value={city}>{city}</option> 
                                )       
                            })}
                    </select>
</div>
<div class="form-group col-md-2">
  <label for="inputZip">Pin Code</label>
  <select name="pincode" required value={vendor.pincode} class="form-control" onChange={handleChange}>
                            <option value="">--Select Pincode--</option>
                            {pincodes.map((pin)=>{
                                return(
                                    <option value={pin}>{pin}</option> 
                                )       
                            })}
                    </select>
</div>
</div>
<button type="submit" class="btn btn-primary">Register</button>
<div>{message}</div>
</form>
</div>
)
}
export default VendorReg;