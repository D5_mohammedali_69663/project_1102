import axios from "axios";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

function CustomerOrders(){
    var i =  1;
    var history = useHistory();
    var[orders, setOrders] = useState([
        {
            id: 2,
            creat_date: "2023-03-02",
            book_date: "2023-03-06",
            book_status: "PENDING",
            order_status: "PENDING",
            servName: "CATERING",
            serv_price: 1000.0,
            cmp_name: "Vance refrigeration",
            vemail: "abhi@a.com"
        }
    ]);


    //on page load bring in all orders
    useEffect(()=>{
        var url = "http://localhost:8080/Project/customer/p/orders/" + sessionStorage.getItem("email");
        axios.get(url)
        .then((response)=>{
            var list = response.data;
            setOrders(list);
        })
        .catch((err)=>{
            console.log(err);
        })
    },[])


    //update order function
    var updateOrder=(args)=>{
        debugger;
        var url = "http://localhost:8080/Project/customer/p/orders/"+args.target.value;
        axios.put(url)
        .then((response)=>{
            debugger;
            var result = response.data;
            debugger;
            if(result==true){
                window.location.reload();
                debugger;
            }else{
                alert(result);
                // setTimeout(()=>{message=""},3000);
                debugger;
            }
        })
    };

    //cancel order function
    var cancelOrder=(args)=>{
        var url = "http://localhost:8080/Project/customer/p/order/"+args.target.value;
        axios.delete(url)
        .then((resposne)=>{
            if(resposne.data){
                window.location.reload();
            }
        })
    }

    var goBack=()=>{
        history.goBack();
    }


    return (<div className="p-4 mb-1 mx-2">
    <h1><b><i>My Orders</i></b></h1>
    <button onClick={goBack} class="btn btn-warning my-2">Back</button>
    <table class="table">
    <thead class="thead-dark">
    <tr class="table-primary">
    <th scope="col">Id</th>
    <th scope="col">Service Name</th>
    <th scope="col">Created On</th>
    <th scope="col">Booked For</th>
    <th scope="col">Booking Status</th>
    <th scope="col">Completion Status</th>
    <th scope="col">Price</th>
    <th scope="col">Provider</th>
        </tr>
        </thead>
        {orders.map((o) => {
            return (
                    <tbody>
                <tr id={o.servName}>
                <th class="table-bg-transparent" scope="row">{i++}</th>
                    
                    <td class="table-bg-transparent"><b>{o.servName}</b></td>
                    <td class="table-bg-transparent"><b>{o.creat_date}</b></td>
                    <td class="table-bg-transparent"><b>{o.book_date}</b></td>
                    <td class="table-bg-transparent"><b>{o.book_status}</b></td>
                    <td class="table-bg-transparent"><b>{o.order_status}</b></td>
                    <td class="table-bg-transparent"><b>{o.serv_price}</b></td>
                    <td class="table-bg-transparent"><b>{o.cmp_name}</b></td>
                    
                    {/* <td><button value={o.servName} onClick={deleteService}>delete</button></td> */}
                    <td><button class="btn btn-success mx-1" onClick={updateOrder} value={o.id}>Confirm Booking</button></td>
                    <td><button class="btn btn-danger mx-1" onClick={cancelOrder} value={o.id}>Cancel</button></td> 
                </tr>
                </tbody>
                        
                
            )
        })}
    </table>
    
</div>)
}

export default CustomerOrders;