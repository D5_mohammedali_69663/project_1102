import Home from './home';
import { Route } from "react-router-dom";

function ProtectedCustomer(props){
    
    var isLoggedIn = sessionStorage.getItem("isLoggedIn");
    var role = sessionStorage.getItem("role");

   if(isLoggedIn && role=="ROLE_CUSTOMER") //check for sessionStorage values
//    if(true)
    {
        return <Route  path={props.path} exact   
                        component={props.component} />;
    }
    else
    {
       return <Home></Home>
    }
}

export default ProtectedCustomer;