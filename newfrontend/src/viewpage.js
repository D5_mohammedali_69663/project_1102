import img1 from './img6.jpg';
import img2 from './img9.jpg';
import img3 from './22.avif';
import venu from './venu.jpg';
import photoshoot from './photoshoot.jfif';
import deco_img from './d.jpg';
import d from './vvback.jpg'
import Card from 'react-bootstrap/Card';
import Carousel from 'react-bootstrap/Carousel';
import { Col, Row } from 'reactstrap';


function ViewPage() {
    return (
        <div height={70} width={70} className=" mb-1 mx-2 'container-fluid'" style={{background: `url(${d})`, zoom: "88%"}}>
      <Carousel>
        <Carousel.Item interval={1000}>
          <img
            className="d-block w-100"
            src={img1}
            alt="First slide"
          />
          <Carousel.Caption>
          <h3><i>“Love at first sight is easy to understand; it’s when two people have been looking at each other for a lifetime that it becomes a miracle.”</i></h3>
            <h1>Sam Levenson</h1>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={1000}>
          <img
            className="d-block w-100"
            src={img2}
            alt="Second slide"
          />
          <Carousel.Caption>
           <h3><i>“Marriage is like watching the color of leaves in the fall; ever changing and more stunningly beautiful with each passing day.”</i></h3>
           <h1>Fawn Weaver</h1>
            
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={1000}>
          <img
            className="d-block w-100"
            src={img3}
            alt="Third slide"
          />
          <Carousel.Caption>
            <h3><i>“A good marriage is one which allows for change and growth in the individuals and in the way they express their love.”</i></h3>
            <h1>Pearl S. Buck</h1>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
      <Row>
        <Col>
      <Card style={{ width: '15rem'}} className="p-1 mt-4 mx-2">
      <Card.Img variant="top" src={deco_img} />
      <Card.Body>
        <Card.Title>Decor</Card.Title>
        <Card.Text>
          Decorations
        </Card.Text>
        {/* <Button variant="dark">Go somewhere</Button> */}
      </Card.Body>
    </Card>
    </Col>
    <Col>
    <Card style={{ width: '15rem'}} className="p-1 mt-4 mx-2">
      <Card.Img variant="top" src={venu} />
      <Card.Body>
        <Card.Title>Venue</Card.Title>
        <Card.Text>
          Venues
        </Card.Text>
        {/* <Button variant="dark">Go somewhere</Button> */}
      </Card.Body>
    </Card>
    </Col>
    <Col>
    <Card style={{ width: '15rem'}} className="p-1 mt-4 mx-2">
      <Card.Img variant="top" src={photoshoot} />
      <Card.Body>
        <Card.Title>Photoshoot</Card.Title>
        <Card.Text>
          Photography
        </Card.Text>
        {/* <Button variant="dark">Go somewhere</Button> */}
      </Card.Body>
    </Card>
    </Col>
    </Row>
    </div>
    );
  }
  
  export default ViewPage;