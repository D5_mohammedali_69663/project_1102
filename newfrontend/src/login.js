import { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from 'axios';
import jwt_decode from "jwt-decode";

function Login(){
    //user credentials
    var[usercred, setUsercred] = useState({email:"", password:""});
    
    var history = useHistory();

    //input text handlechange function
    var handleChange=(args)=>{
        var copyOfusercred = {...usercred};
        copyOfusercred[args.target.name] = args.target.value;
        setUsercred(copyOfusercred);
    }
    var jwt;

    var Authenticate=()=>{
        debugger;
        var data = {...usercred};

        axios.post("http://localhost:8080/Project/authenticate", data)
        .then(function (response) {
            debugger;
            jwt = response.data;
            if(jwt != null){
                
                var decoded = jwt_decode(jwt);
                var email = decoded.email;
                var role = decoded.role[0].authority;
                sessionStorage.setItem("email", email);
                sessionStorage.setItem("role", role);
                sessionStorage.setItem("Authorization", jwt);
                sessionStorage.setItem("isLoggedIn", true);
                debugger;
                console.log(decoded);
                if(role=="ROLE_ADMIN"){
                  history.push("/admin");
                }else if(role=="ROLE_VENDOR"){
                  history.push("/vendor");
                }else if(role=="ROLE_CUSTOMER"){
                  history.push("/customer")
                }
            }

          })
          .catch(function (error) {
            var er = error;
            // debugger;
            alert(error.message);
            console.log(error);
          });

        // var copyOfusercred = {...usercred};
        // copyOfusercred={email:"", password:""};
        // setUsercred(copyOfusercred);
    }




    return(<div className="p-5">
      <div style={{width: 600}}>
        <h1><b><i>Login Page</i></b></h1>
        {/* <form onSubmit={Authenticate}>   */}
        {/* <form> */}
  
  <div class="form-outline mb-4 my-3">
    <input type="email" name="email" class="form-control bg-transparent" placeholder="Email" value={usercred.email} required onChange={handleChange}/>
    <label class="form-label" for="form2Example1"><b>Email Address</b></label>
  </div>

 
  <div class="form-outline mb-4">
    <input type="password" name="password" class="form-control bg-transparent" placeholder="Password" value={usercred.password} required onChange={handleChange}/>
    <label class="form-label" for="form2Example2"><b>Password</b></label>
  </div>

  
  <div class="row mb-4">
    <div class="col d-flex justify-content-center">
      
      {/* <div class="form-check">
        <input class="form-check-input" type="checkbox" value="" id="form2Example31" checked />
        <label class="form-check-label" for="form2Example31"> Remember me </label>
      </div> */}
      
    </div>

    {/* <div class="col">
      
      <a href="#!">Forgot password?</a>
    </div> */}
  </div>
  

  
  <button type="submit" class="btn btn-primary btn-block mb-4" onClick={Authenticate}>Login</button>
  <a href="/customerReg"> <button type="submit"  class="btn btn-warning btn-block mb-4">
   Customer Registeration</button></a>
  <a href="/vendorReg"> <button type="submit"  class="btn btn-warning btn-block mb-4">
   Vendor Registeration</button></a>
  
  {/* <div class="text-center">
    <p>Not a member? <a href="#!">Customer Register</a></p>
    <p>Not a member? <a href="#!">Vendor Register</a></p>
    
  </div> */}
{/* </form> */}
        {/* </form> */}
        </div>
    </div>)
}

export default Login;