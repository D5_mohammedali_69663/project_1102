import axios from "axios";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";



function CutstomerBrowse() {
    var i = 1;
    var [browse, setBrowse] = useState([
        {
            id:0,
            servName: "",
            serv_desc: "",
            serv_price: 0,
            creatn_date: "",
            first_name: "",
            cmp_name: "",
            pincode:0 ,
            city: "",
            state: "",
            mob_no: 0,
            email: ""
        }
    ]);
    
    var history = useHistory();

    useEffect(()=>{
        var url = "http://localhost:8080/Project/customer/services";
        axios.get(url)
        .then((response)=>{
            var list = response.data;
            setBrowse(list);
        })
    },[])

    //function to view Service, behave api call
    var viewService =(args)=>{
        var behaveUrl = "http://localhost:8080/Project/customer/behaviour/"+sessionStorage.getItem("email")+"/"+args.target.value;
        axios.post(behaveUrl)
        .then((response)=>{
            var res = response.data;
        })
        .catch((err)=>{
            console.log(err);
        })
        var sid = args.target.value;
        history.push({
            pathname: "/customer/browse/product",
            state: {sid}
            
        });
    }

    //go back function
    var goBack=()=>{
        history.goBack();
    }

    return (<div className="p-5 mb-1 mx-4">
    <h1><b><i>Customer Services</i></b></h1>

        
        <button onClick={goBack} class="btn btn-warning my-2">Back</button>
        <table class="table">
  <thead class="thead-dark">
    <tr class="table-primary">
      <th scope="col">Sr No.</th>
      <th scope="col">Service Name</th>
      <th scope="col">Discription</th>
      <th scope="col">City</th>
      <th scope="col">State</th> 
      <th scope="col">Price Per Unit</th>
    </tr>
  </thead>
            {browse.map((o) => {
                return (
                    <tbody>
        <tr id={o.servName}>
        <th class="table-bg-transparent" scope="row">{i++}</th>
        <td class="table-bg-transparent"><b>{o.servName}</b></td>
        <td class="table-bg-transparent"><b>{o.serv_desc}</b></td>
        <td class="table-bg-transparent"><b>{o.city}</b></td>
        <td class="table-bg-transparent"><b>{o.state}</b></td>
        <td class="table-bg-transparent"><b>{o.serv_price}</b></td>
        <td><button onClick={viewService} class="btn btn-info" value={o.id}>View</button></td>
      </tr>
    </tbody>

                )
            })}

        </table>
    </div>)

}

export default CutstomerBrowse;