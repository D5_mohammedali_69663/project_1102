import { useState, useEffect } from "react";
import axios from 'axios';
import { useHistory } from "react-router-dom";

import state_city from './resources/state_city.json'

function VendorReg(){

    // history
    var history = useHistory();


    var[vendor, setVendor] = useState({
        email: "",
        password: "",
        first_name: "",
        last_name: "",
        mob_no: "",
        dob: "",
        gender: "",
        pincode:431006,
        cmp_name:""
    });


     //On Fail message
     var [message, setMessage] = useState();
    
     //array of list of states
     var[statesList, setStatesList] = useState([]);
     //array of list of cities
     var [cityList, setCityList] = useState([]);  
     // array of list of pincodes
     var [pincodes, setPincodes] = useState([]);    



 


    //OnChange handler
    var handleChange=(args)=>{
        var copyOfvendor = {...vendor};
        copyOfvendor[args.target.name] = args.target.value;
        setVendor(copyOfvendor);
    }





    //on mount getting list of states
    useEffect(()=>{
        //call to get list of states
        debugger;
        getState();
           
    },[]);

    var getState = ()=>{
        axios.get("http://localhost:8080/Project/aux/state")
             .then(function(response){
                var states = response.data;
                setStatesList(states);
                // debugger;
             })
    }
    //On state select getting citylist
    var getCity = (args)=>{
        var statesSelected = args.target.value;
        var cityUrl = "http://localhost:8080/Project/aux/city/" + statesSelected;
        // debugger;
        axios.get(cityUrl)
             .then(function(response){
                var cities = response.data;
                setCityList(cities);
                // debugger;
             }) 
    }

    //On change city list
    var getPincode = (args)=>{
        var citySelected = args.target.value;
        var pinUrl = "http://localhost:8080/Project/aux/pincode/" + citySelected;
        // debugger;
        axios.get(pinUrl)
             .then(function(response){
                var pincode = response.data;
                setPincodes(pincode);
                // debugger;
             }) 
    }



    var onRegister;


    //On submit Register Function
    var Register=()=>{
        debugger;
        var cData = {...vendor};
        axios.post("http://localhost:8080/Project/register/vendor",cData)
        .then((response)=>{
            onRegister = response.data;
            debugger;
        }).catch((err)=>{
            console.log(err);
        })

        if(onRegister!=null && onRegister == "true"){
            history.push("/home");  
            
        }else{            
            setMessage(onRegister);
            debugger;
        }
    }

    return(<div className="p-4 mb-3 mx-5">
    <h1><b><i>Vendor Registration Page</i></b></h1>
    <div style={{width: 600}}>
    {/* <form > */}
<div class="form-row my-3">
<div class="form-group ">
  <label for="inputEmail4"><b>Email</b></label>
  <input type="email" class="form-control bg-transparent" name ="email" id="inputEmail4" placeholder="Email" required value={vendor.email} onChange={handleChange}/>
</div>
<div class="form-group ">
  <label for="inputPassword4"><b>Password</b></label>
  <input type="password" class="form-control bg-transparent" name ="password" id="inputPassword4" placeholder="Password" required value={vendor.password} onChange={handleChange}/>
</div>
</div>
<div class="form-group">
<label for="inputAddress"><b>First Name</b></label>
<input type="text" class="form-control bg-transparent" name ="first_name" id="inputAddress" placeholder="First Name" required value={vendor.first_name} onChange={handleChange}/>
</div>
<div class="form-group">
<label for="inputAddress2"><b>Last Name</b></label>
<input type="text" class="form-control bg-transparent" name ="last_name" id="inputAddress2" placeholder="Last Name" required value={vendor.last_name} onChange={handleChange}/>
</div>
<div class="form-group">
<label for="inputAddress2"><b>Company Name</b></label>
<input type="text" class="form-control bg-transparent" name ="cmp_name" id="inputAddress2" placeholder="Company Name" required value={vendor.cmp_name} onChange={handleChange}/>
</div>
<div class="form-group">
<label for="inputAddress2"><b>Mobile No.</b></label>
<input type="text" class="form-control bg-transparent" name ="mob_no" id="inputAddress2" placeholder="Mobile No" pattern="^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$" required value={vendor.mob_no} onChange={handleChange}/>
</div>
<div class="form-group">
<label for="inputAddress2"><b>Date Of Birth</b></label>
<input type="date" class="form-control bg-transparent" name ="dob" id="inputAddress2" required value={vendor.dob} onChange={handleChange}/>
</div>
<div class="form-row">
<div class="form-group ">
  <label for="inputState"><b>Gender</b></label>
  <select name="gender" required value={vendor.gender} class="form-control bg-transparent" onChange={handleChange}>
                            <option value="">--Select Gender--</option>
                            <option value="MALE">Male</option>
                            <option value="FEMALE">Female</option>
                            <option value="OTHER">Other</option>
                        </select>
</div>
<div class="form-group ">
  <label for="inputCity"><b>State</b></label>
  <select name="state"  class="form-control bg-transparent" required onChange={getCity}>
                            <option value="">--Select State--</option>
                            {statesList.map((state)=>{
                                return(
                                    <option value={state}>{state}</option> 
                                )   
                            })}
                        </select>
</div>
<div class="form-group ">
  <label for="inputCity"><b>City</b></label>
  <select name="city"  class="form-control bg-transparent" required onChange={getPincode}>
                            <option value="">--Select City--</option>
                            {cityList.map((city)=>{
                                return(
                                    <option value={city}>{city}</option> 
                                )       
                            })}
                    </select>
</div>
<div class="form-group ">
  <label for="inputZip"><b>Pin Code</b></label>
  <select name="pincode" required value={vendor.pincode} class="form-control bg-transparent" onChange={handleChange}>
                            <option value="">--Select Pincode--</option>
                            {pincodes.map((pin)=>{
                                return(
                                    <option value={pin}>{pin}</option> 
                                )       
                            })}
                    </select>
</div>
</div>
<button type="submit"  class="btn btn-primary my-3">Register</button>
<div>{message}</div>
{/* </form> */}
</div>
</div>
)
}
export default VendorReg;