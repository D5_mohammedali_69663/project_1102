import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import '../node_modules/bootstrap/dist/js/bootstrap.min.js'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './chat.css';

function VendorMail(){


    var currentUser = sessionStorage.getItem("email");
    var [vmail, setVmail] = useState();

    var[chats, setChats] = useState([]);

    var[msg, setMsg] = useState({
        custEmail:"",
        vendEmail:"",
        message:""
    });

    var history = useHistory();
   

    //handle change funciton
    var handleChange = (args)=>{
        var copyOfMsg = {...msg};
        copyOfMsg[args.target.name] = args.target.value;
        setMsg(copyOfMsg);
    }

    //send message function
    var sendMessage=()=>{
        var url = "http://localhost:8080/Project/vendor/mail/"+sessionStorage.getItem("email");
        var data = {...msg};
        data.custEmail = vmail;
        data.vendEmail = sessionStorage.getItem("email");
        debugger;
        axios.post(url, data)
        .then((response)=>{
            var res = response.data;
            if(res==true){
                window.location.reload();
            }else{
                alert(res);
            }
        })
    }

    var [messages, setMessages] = useState([
        {
            id: 0,
            custEmail: "",
            vendEmail: "",
            sender: "",
            sentDate: "",
            message: "",
            readStatus: ""
        }
    ])


    useEffect(() => {
        var url = "http://localhost:8080/Project/vendor/chats/"+sessionStorage.getItem("email");
        axios.get(url)
        .then((response)=>{
            var list = response.data;
            setChats(list);
        })
        .catch((err)=>{
            console.log(err);
        })
    }, []);

    var goBack=()=>{
        history.goBack();
    }

    var bringChats=(args)=>{
        var vmail = args.target.value;
        setVmail(vmail);
        var url = "http://localhost:8080/Project/vendor/messages/"+vmail+"/"+sessionStorage.getItem("email");
        debugger;
        axios.get(url)
        .then((response)=>{
            debugger;
            var list = response.data;
            setMessages(list);
            debugger;
        })
        .catch((err)=>{
            debugger;
            console.log(err);
        })
    }


    return (<div className="p-4 mb-3 mx-5">
        <h1><b><i>Vendor Mailbox</i></b></h1>
        <button class="btn btn-dark mx-4" onClick={goBack}>Back</button>  
    <div id="chatBox" className="row">
        
        <div className="col-2 border-dark bg-transparent" id="chats">
            {chats.map((c) => {
                return (
                    <button id={c} className="chats" value={c} onClick={bringChats}>
                        {c}
                    </button>
                )
            })}
        </div>
        <div className="col-1 border-dark bg-transparent">

        </div>
        <div className="col-5 row box" id="messages">
            <div class="form-control bg-transparent" className="col-12">{messages.vendEmail}</div>
            {messages.map((m) => {
                if (m.sender != currentUser) {
                    return (<span className="right">
                        {m.message}
                    </span>
                    )
                } else {
                    return (<span className="left">
                        {m.message}
                    </span>)
                }
            })}
            <div className="m-3"><input type="text" className=" col-11 form-control bg-transparent" name="message" value={msg.message} onChange={handleChange}></input><button class="btn btn-warning" onClick={sendMessage}>Send</button></div>
        </div>
        
    </div>
    </div>)

}


export default VendorMail;